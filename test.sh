#!/bin/sh

./raw-converter 250000 test-output.raw test-input.jpg && \
diff -b test-output.raw test-output-reference.raw && \
./raw-converter 0 test-output-0.raw test-input.jpg && \
diff -b test-output-0.raw test-output-reference-0.raw && \
echo OK
