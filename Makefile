VERSION=`git describe --always`

# Used to build debian package
# here we consider that artifacts are already built (to speed up continuous integration)
# so we do nothing here
build:
	./compile.sh && ./test.sh

# Build debian package
deb:
	dch -v ${VERSION} "git update, version ${VERSION}"
	bash .dh_build.sh
	mv ../raw-converter*.deb .

# Debian package install
install:
	rm -rf debian/raw-converter/usr && mkdir -p debian/raw-converter/usr/bin
	cp raw-converter debian/raw-converter/usr/bin

clean:
	#debclean
