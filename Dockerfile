FROM debian:bookworm

LABEL maintainer="denis.boisset@orange.com & christophe.maldivi@orange.com"


# Do not clean package cache
# which is required for debian package build
# Create "/usr/share/man/man1" manually due to debian-slim issue (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199)
# hadolint ignore=DL3008
RUN mkdir -p /usr/share/man/man1 && rm /etc/apt/apt.conf.d/docker-clean && \
    apt-get update && \
    apt-get install --no-install-recommends -y patch make devscripts fakeroot debhelper libopencv-dev g++ libopencv-core406 git build-essential && \
    rm -rf /var/lib/apt/lists/*
