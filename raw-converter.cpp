#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

void serializeMatbin(cv::Mat& mat, std::string filename);

int main(int argc, const char** argv)
{

    if (argc !=  4) {
        printf("Usage raw-converter pCount fileDest fileSource\n");
        return 1;
    }

    long nbPixelsTarget = atol( argv[1]);
    Mat LoadedImage = imread(argv[3], IMREAD_COLOR);
    if (nbPixelsTarget > 0) {
        long matPixelsCount = LoadedImage.total();
        double ratio = (double)matPixelsCount / nbPixelsTarget;
        double sqrtRatio = (double)sqrt(ratio);
        int newWidth = (int) (LoadedImage.size().width / sqrtRatio);
        int newHeight = (int) (LoadedImage.size().height / sqrtRatio);
        Mat newMat = Mat(newHeight, newWidth, LoadedImage.type());
        resize(LoadedImage, newMat, cv::Size(newMat.size()), sqrtRatio, sqrtRatio, cv::INTER_LANCZOS4);
        waitKey(1000);
        serializeMatbin(newMat, argv[2]);
     } else {
        serializeMatbin(LoadedImage, argv[2]);
     }
}


void serializeMatbin(cv::Mat& mat, std::string filename){
 	int elemSizeInBytes = (int)mat.elemSize();
	int elemType = (int)mat.type();
	int dataSize = (int)(mat.cols * mat.rows * mat.elemSize());

	FILE* FP = fopen(filename.c_str(), "wb");
	for(int i=0; i< mat.rows; i++) {
	    unsigned char *ptr = mat.data + i * mat.cols *elemSizeInBytes;
		fwrite(ptr, mat.cols, elemSizeInBytes, FP);
		fwrite("\n", sizeof(char), 1, FP);
	}

	fclose(FP);
}
